<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use App\Mail\ContactEmail;

use App\Rules\Captcha;

use App\Models\Play;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['pageTitle'] = 'Playwright Neal';
        $data['metaKeywords'] = '';
        $data['metaDescription'] = '';

        $data['plays'] = Play::orderBy('title')->get();

        return view('home', $data);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function about()
    {
        $data['pageTitle'] = 'Playwright Neal | About';
        $data['metaKeywords'] = '';
        $data['metaDescription'] = '';
        
        return view('about', $data);
    }

    /**
     * Show the contact page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function contact()
    {
        $data['pageTitle'] = 'Playwright Neal | Contact';
        $data['metaKeywords'] = '';
        $data['metaDescription'] = '';

        return view('contact', $data);
    }

    /**
     * Show the plays page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function plays()
    {
        $data['pageTitle'] = 'Playwright Neal | Plays';
        $data['metaKeywords'] = '';
        $data['metaDescription'] = '';

        $data['plays'] = Play::orderBy('title')->get();

        return view('plays', $data);
    }

    /**
     * Show the resources page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function resources()
    {
        $data['pageTitle'] = 'Playwright Neal | Resources';
        $data['metaKeywords'] = '';
        $data['metaDescription'] = '';

        return view('resources', $data);
    }

    /**
     * Show the thank you page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function thanks()
    {
        $data['pageTitle'] = 'Neal Lewis | Thank You';
        $data['metaKeywords'] = 'thank you for submission';
        $data['metaDescription'] = '';

        return view('thanks', $data);
    }

    /**
     * Process the contact form. 
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function processContact(Request $request)
    {
        $mailTo = 'blackyin@gmail.com';

        $data = new \stdClass();

        $rules = [
            'name' => 'required|string|max:255',
            'email' => 'required|email',
            'message' => 'required|string',
        ];

        $rules['g-recaptcha-response'] = new Captcha();

        $this->validate($request, $rules);

        $data->name = $request->input('name');
        $data->email = $request->input('email');
        $data->message = $request->input('message');

        Mail::to($mailTo)->send(new ContactEmail($data));

        return redirect(route('thanks'));
    }   
}

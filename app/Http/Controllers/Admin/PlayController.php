<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Play;

class PlayController extends Controller
{
	/**
	 * Show an index of plays.
	 * 
	 * @return \Illuminate\Http\Response
	 */
    public function index()
    {
    	$data['plays'] = Play::orderBy('title')->get();

    	return view('admin.plays.index', $data);
    }

    /**
     * Show the form for creating a new play. 
     * 
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	return $this->edit(new Play());
    }

    /**
     * Store a newly created play. 
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	return $this->update($request, new Play());
    }

    /**
     * Show the form for editing or creating a play.
     * 
     * @param  \App\Models\Play   $play
     * @return \Illuminate\Http\Response
     */
    public function edit(Play $play)
    {
    	$this->authorize('update', $play);

    	$data['play'] = $play;

    	return view('admin.plays.edit', $data);
    }

    /**
     * Update the given play in storage. 
     * 
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Play         $play
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Play $play)
    {
    	$this->authorize('update', $play);

    	$this->validate($request, [
    		'title' => 'required|string|max:255',
    		'text' => 'required|string',
    		'details' => 'required|string|max:255',
            'link' => 'nullable|string|max:255',
    	]);

    	$play->title = $request->input('title');
    	$play->text = $request->input('text');
    	$play->details = $request->input('details');
        $play->link = $request->input('link');

    	$saved = $play->save();

    	if ($saved) {
    		return redirect(route('admin.plays.edit', $play))->with('success', 'Play has been saved.');
    	} else {
    		return redirect(route('admin.plays.edit', $play))->with('danger', 'Something went wrong.');
    	}
    }

    /**
     * Delete a play in storage. 
     * 
     * @param  \App\Models\Play   $play
     * @return \Illuminate\Http\Response
     */
    public function destroy(Play $play)
    {
    	$this->authorize('destroy', $play);

    	$deleted = $play->delete();

    	if ($deleted) {
    		return redirect(route('admin.plays.index'))->with('success', 'Play has been deleted.');
    	} else {
    		return redirect(route('admin.plays.index'))->with('danger', 'Something went wrong.');
    	}
    }
}

<?php

namespace App\Policies;

use Auth;

use App\Models\Play;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PlayPolicy
{
    use HandlesAuthorization;

    /**
     * Allow logged in users to update plays.
     * 
     * @param  \App\Models\User   $user
     * @param  \App\Models\Play   $play
     * @return Boolean
     */
    public function update(User $user, Play $play)
    {
        return $user === Auth::user();
    }

    /**
     * Allow logged in users to destroy plays.
     * 
     * @param  \App\Models\User   $user
     * @param  \App\Models\Play   $play
     * @return Boolean
     */
    public function destroy(User $user, Play $play)
    {
        return $user === Auth::user();
    }
}

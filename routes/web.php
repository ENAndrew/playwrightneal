<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/register', 'Auth\RegisterController@abort')->name('register');
Route::post('/register', 'Auth\RegisterController@abort')->name('register.post');

Route::get('/', 'HomeController@index')->name('home');

Route::get('/plays', 'HomeController@plays')->name('plays');

Route::get('/about', 'HomeController@about')->name('about');

Route::get('/contact', 'HomeController@contact')->name('contact');
Route::post('/contact', 'HomeController@processContact')->name('process.contact');

Route::get('/resources', 'HomeController@resources')->name('resources');

Route::get('/thanks', 'HomeController@thanks')->name('thanks');

Route::group(['middleware' => 'auth', 'prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.'], function() {
	Route::get('/', 'AdminController@dashboard')->name('dashboard');

	Route::resource('users', 'UserController');

	Route::resource('plays', 'PlayController');
});


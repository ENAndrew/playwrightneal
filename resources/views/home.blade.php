@extends ('layouts.app')

@section ('content')
	<div class="home-wrapper">
		<section class="statement clearfix pt-5 pb-5">
			<picture>
				<source srcset="/img/neal.webp" type="image/webp">
				<img src="/img/neal.jpg" class="img-fluid pull-left mr-3 mb-3" alt="Neal Lewis">
			</picture>

			<h1>Artist Statement</h1>

			<p>Theatre has the potential to inspire hope through such well crafted and humanistic characters as have come to embrace their lives in spite of their woe. It is Mr. Lewis' hope that by presenting such characters, people may be inspired to embrace their own disparate lives, and that we all might find greater understanding, awareness and acceptance for the tribulations of others' experiences despite the confines of our own.</p>

			<p>It is Mr. Lewis' hope that his work will stand as a testament to our shared humanity and reflect his advocacy for a better world for us all. Mr. Lewis believes that in reviving Verse Drama, we can facilitate Empathy for the Other by the juxtaposition of the modern era and the cadence of the ancient arts as we embrace our differences and explore the nuances of sex, gender, race and belief.</p>

			<p>A member of the Dramatists Guild, Neal Alexander Lewis has been writing Verse Drama since 2001. His long relationship with the theatre has found him both onstage and backstage, as well as volunteering as House Manager for The Oceanside Theatre Company. Mr. Lewis is currently working on his next project.</p>
		</section>

		<section class="works">
			<h1>Works</h1>

			@if ($plays->count())
				<div class="plays">
					@foreach ($plays as $play)
						<div class="play-box">
							<div class="play-padding">
								<h3 class="text-right">{{ $play->title }}</h3>

								{!! $play->text !!}

								<p class="details text-right">{{ $play->details }}</p>

								@if ($play->link)
									<p class="text-right">
										<a href="{{ $play->link }}" target="_blank">Read <i class="fa fa-long-arrow-right"></i></a>
									</p>
								@endif
							</div>
						</div>
					@endforeach
				</div>
			@endif 
		</section>
	</div>
@endsection
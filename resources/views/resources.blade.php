@extends ('layouts.app')

@section ('content')
	<div class="resources-wrapper">
		<section class="header text-center">
			<h1>Resources</h1>
		</section>

		<section class="resources">
			<div class="row">
				<div class="col-lg-6">
					<h2 class="text-center">For if you need help:</h2>

					<div class="resource">
						<a target="_blank" href="https://www.rainn.org/">
							<h5>RAINN</h5>
						</a>

						<p>Rape, Abuse, and Incest National Network</p>
					</div>

					<div class="resource">
						<a target="_blank" href="https://nomore.org/">
							<h5>No More</h5>
						</a>

						<p>Together we can end domestic violence and sexual assault</p>
					</div>

					<div class="resource">
						<a target="_blank" href="https://www.glaad.org/">
							<h5>GLAAD</h5>
						</a>
					</div>

					<div class="resource">
						<a target="_blank" href="https://suicidepreventionlifeline.org/">
							<h5>Suicide Prevention Lifeline</h5>
						</a>

						<p>
							<a href="tel:18002738255">800 273 8255</a>
						</p>
					</div>

					<div class="resource">
						<a target="_blank" href="https://www.nimh.nih.gov/index.shtml">
							<h5>NIMH</h5>
						</a>

						<p>National Institue of Mental Health</p>
					</div>

					<div class="resource">
						<a target="_blank" href="https://afsp.org/">
							<h5>AFSP</h5>
						</a>

						<p>American Foundation for Suicide Prevention</p>
					</div>

					<div class="resource">
						<a target="_blank" href="https://www.naacp.org/">
							<h5>NAACP</h5>
						</a>

						<p>National Associate for the Advancement of Colored People</p>
					</div>

					<div class="resource">
						<a target="_blank" href="https://www.nationaleatingdisorders.org/help-support">
							<h5>NEDA</h5>
						</a>

						<p>National Eating Disorders Association</p>
					</div>
				</div>

				<div class="col-lg-6">
					<h2 class="text-center">Playwright Related:</h2>

					<div class="resource">
						<a target="_blank" href="https://www.dramatistsguild.com/">
							<h5>Dramatists Guild</h5>
						</a>
					</div>

					<div class="resource">
						<a target="_blank" href="https://sandiegoplaywrights.wordpress.com/">
							<h5>San Diego Playwrights</h5>
						</a>
					</div>

					<div class="resource">
						<a target="_blank" href="https://newplayexchange.org/">
							<h5>New Play Exchange</h5>
						</a>
					</div>
				</div>
			</div>
		</section>
	</div>
@endsection
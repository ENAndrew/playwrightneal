<nav class="navbar navbar-expand-lg">
	<a class="navbar-brand" href="/">Neal Lewis</a>

	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-links">
		<i class="fa fa-bars"></i>
	</button>

	<div class="collapse navbar-collapse" id="nav-links">
		<div class="navbar-nav">
			<a class="nav-item nav-link {{ ActiveRoute::startsWith('plays') }}" href="{{ route('plays') }}">Plays</a>

			<a class="nav-item nav-link {{ ActiveRoute::startsWith('about') }}" href="{{ route('about') }}">About</a>

			<a class="nav-item nav-link {{ ActiveRoute::startsWith('contact') }}" href="{{ route('contact') }}">Contact</a>

			<a class="nav-item nav-link {{ ActiveRoute::startsWith('resources') }}" href="{{ route('resources') }}">Resources</a>
		</div>
	</div>

	<span class="navbar-text">Verse Drama for the Modern Age</span>
</nav>
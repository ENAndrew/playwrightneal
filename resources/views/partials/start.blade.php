<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>{{ $pageTitle ?? config('app.name', 'Playwright Neal') }}</title>

	<meta name="keywords" content="{{ $metaKeywords ?? '' }}">
	<meta name="description" content="{{ $metaDescription ?? '' }}">

	<script src="{{ asset('js/app.js') }}" defer></script>

	<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v5.0&appId=229760990717206&autoLogAppEvents=1"></script>

	<link rel="dns-prefetch" href="//fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans|Special+Elite&display=swap" rel="stylesheet">

	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
	<div id="app">
		
	
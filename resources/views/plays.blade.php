@extends ('layouts.app')

@section ('content')
	<div class="plays-wrapper">
		<section class="works">
			@if ($plays->count())
				<div class="plays">
					@foreach ($plays as $play)
						<div class="play-box">
							<div class="play-padding">
								<h3 class="text-right">{{ $play->title }}</h3>

								{!! $play->text !!}

								<p class="details text-right">{{ $play->details }}</p>

								@if ($play->link)
									<p class="text-right">
										<a href="{{ $play->link }}" target="_blank">Read <i class="fa fa-long-arrow-right"></i></a>
									</p>
								@endif
							</div>
						</div>
					@endforeach
				</div>
			@endif 
		</section>
	</div>
@endsection
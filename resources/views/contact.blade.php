@extends ('layouts.app')

@section ('content')
	<div class="contact-wrapper">
		<section class="contact pb-5">
			<h1>Contact Neal</h1>

			@include ('partials.alerts')

			<form class="contact-form" method="POST">
				@csrf

				<div class="form-group">
					<label for="name">Your Name:</label>

					<input class="form-control" type="text" name="name" value="{{ old('name') }}">
				</div>

				<div class="form-group">
					<label for="email">Email:</label>

					<input class="form-control" type="email" name="email" value="{{ old('email') }}">
				</div>

				<div class="form-group">
					<label for="message">Message:</label>

					<textarea class="form-control" rows="6" name="message">{{ old('message') }}</textarea>
				</div>

				<div class="g-recaptcha" data-sitekey="{{ config('recaptcha.key') }}"></div>

				<input type="button" class="btn btn-primary mt-3" value="Submit" onclick="this.disabled=true;this.value='Submitting, please wait...';this.form.submit();">
			</form>
		</section>

		<section class="facebook pb-5">
			<a href="https://www.facebook.com/Playwrightneal/">
				<h1><i class="fa fa-facebook-official"></i> Connect on Facebook</h1>
			</a>
		</section>
	</div>
@endsection
@extends ('layouts.admin')

@section ('content')
	<div class="play-index-wrapper">
		<div class="d-flex align-items-center justify-content-between">
			<h1>Plays</h1>

			<a href="{{ route('admin.plays.create') }}">
				<button class="btn btn-primary">
					<i class="fa fa-plus"></i> New Play
				</button>
			</a>
		</div>

		@include ('partials.alerts')

		<table class="table mb-5">
			<thead>
				<th>Title</th>

				<th>Actions</th>

				<th>&nbsp</th>
			</thead>

			<tbody>
				@foreach ($plays as $play)
					<tr>
						<td>
							<a href="{{ route('admin.plays.edit', $play) }}">{{ $play->title }}</a>
						</td>

						<td>
							<a href="{{ route('admin.plays.edit', $play) }}">
								<i title="Edit" class="fa fa-edit"></i>
							</a>
						</td>

						<td>
							<form action="{{ route('admin.plays.destroy', $play) }}" method="POST" onsubmit="if(!confirm('Are you sure? This is permanent.')) return false">
								@csrf @method('DELETE')

								<button type="submit" class="btn btn-danger" style="background-color: white; color: red;">
									<i title="Delete" class="fa fa-trash"></i>
								</button>
							</form>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection
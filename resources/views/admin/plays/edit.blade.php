@extends ('layouts.admin')

@section ('content')
<div class="plays-edit-wrapper mb-5">
		@if ($play->id)
			<h1>Edit {{ $play->title }}</h1>
		@else
			<h1>Create New Play</h1>
		@endif

		<form class="basic-form" role="form" action="/admin/plays{{ $play->id ? '/' . $play->id : '' }}" method="POST" autocomplete="force-no">
			@csrf

			@if ($play->id)
				@method('PATCH')
			@endif

			@include ('partials.alerts')

			<div class="form-group">
				<label for="title">Title</label>

				<input id="title" type="text" class="form-control" name="title" value="{{ $play->title ?? old('title') }}">
			</div>

			<div class="form-group">
				<label for="text">Play Text</label>

				<tiny-mce id="text" name="text" value="{{ $play->text ?? old('text') }}"></tiny-mce>
			</div>

			<div class="form-group">
				<label for="details">Details</label>

				<input id="details" type="text" class="form-control" name="details" value="{{ $play->details ?? old('details') }}">
			</div>

			<div class="form-group">
				<label for="link">Link</label>

				<input id="link" type="text" class="form-control" name="link" value="{{ $play->link ?? old('link') }}">
			</div>

            <button class="btn btn-primary" type="submit">Submit/Update Play</button>
		</form>
	</div>
@endsection
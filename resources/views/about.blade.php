@extends ('layouts.app')

@section ('content')
	<div class="about-wrapper">
		<section class="top-image text-center pb-3">
			<picture>
				<source srcset="/img/neal_vinaka.webp" type="image/webp">
				<img src="/img/neal_vinaka.jpg" class="img-fluid" alt="Neal Lewis credit Ashley Bohm">
			</picture>
		</section>

		<section class="info pt-5 pb-5">
			<p>Neal Alexander Lewis found his way into playwrighting by a more circuitous route than many of his peers. Although Mr. Lewis spent much of his time in High School and College in theatre, when he began writing in Junior High, his scribblings chased the lines of High Fantasy novels. Many years would have to pass before a particularly rough year sent Mr. Lewis into the refuge of verse and dialogue. A glorified journal entry, written on purpose, but only incidentally a script, would forever alter the course of his creative expression. His eyes were opened, and he never looked back.</p>

			<p>Mr. Lewis would say he doesn’t know why he writes in verse, only that his brain doesn’t compose plays in any other way. He has devoted years in the development of his voice, at first emulating the heavy cadence of Milton and “Paradise Lost” before realizing how unwieldy Heroic Couplets were in his hands. Each of Mr. Lewis’ earliest plays were developmental steps in the pursuit of a form he could call his own, one he continues to refine even now. Mr. Lewis believes that his particular form of Verse Drama is more fluid and less cumbersome to the modern ear, and in many ways more free than traditional stylings, while still maintaining the aesthetic cadence that makes verse dialogue so compelling.</p>

			<p>Over the same arc of time, Mr. Lewis storytelling has also evolved. They say if it bores the writer, it will bore the audience, and Mr. Lewis has taken that to heart. He learned pretty quickly that he wasn’t interested in dramatizing his own life, but that he was very interested in character dynamics and composition. While not always an intentional direction, Mr. Lewis’ work is, in part, a deconstruction of his own privilege. He endeavors to present genuine characters, not caricatures and stereotypes, and that requires an examination, understanding, and appreciation of voices that are not his.</p>
		</section>
	</div>
@endsection
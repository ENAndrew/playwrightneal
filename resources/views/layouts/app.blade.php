@include ('partials.start')

	<div class="brick-background">
		<div class="wrapper">
			<div class="main-container container">
				@include ('partials.nav')

				<hr>
				
				@yield ('content')
			</div>
		</div>
	</div>

@include ('partials.end')
@include ('partials.start')

	<div class="brick-background">
		<div class="wrapper">
			<div class="main-container container">
				@include ('partials.nav')

				<hr>
				
				<div class="row">
					@include ('admin.partials.sidebar')

					<div class="col-sm-9">
						@yield ('content')
					</div>
				</div>
			</div>
		</div>
	</div>

@include ('partials.end')